(ns flatten.core
  (:require [clojure.spec.alpha :as s]))

;Specification for the expected argument to the alt-flatten function @see https://clojure.org/about/spec
;Defines a recursive spec where valid input is either a vector of integers or a nested vector of integers
(s/def ::int-nested-vector
  (s/coll-of
    (s/or
      :number integer?
      :number-vector ::int-nested-vector)
    :kind vector?)
  )


(defn alt-flatten
  "An alternative to clojure's core/flatten function"
  [nested-vector]

  ;do an initial check of the arguments
  (if (not (s/valid? ::int-nested-vector nested-vector))
    ;if arguments don't conform to the spec throw an error
    (throw (IllegalArgumentException. (s/explain-str ::int-nested-vector nested-vector)))
    ;else  arguments are valid so call a function to recursively flatten the vector
    ((fn inner-flatten-fn [arg]
       (if (not (vector? arg))
         (vector arg)                    ;base case - input is not a vector return vector of the element
         (mapcat inner-flatten-fn arg)))  ;map over the vector elements and concatenate the results of the recursive call
       nested-vector)))
