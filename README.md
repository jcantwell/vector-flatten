# alt-flatten

An alternative to the core/flatten function.
Differences to core/flatten:
 - it only accepts vectors of integers instead of arbitrary sequences.
 - it throws an IllegalArgumentException if the argument passed in is not a vector of integers or a nested vector of integers.

## Usage

(alt-flatten [0 2 4 6])  -> [0 2 4 6]


(alt-flatten [0 [2 4 [6] 8])  -> [0 2 4 6 8]