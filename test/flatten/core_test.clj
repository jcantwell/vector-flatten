(ns flatten.core-test
  (:require [clojure.test :refer :all]
            [clojure.spec.test.alpha :as t]
            [flatten.core :refer :all]))



(defn generate-nested-vector
  "Fixture function to create a nested vector"
  [base-vector dept]
  (reduce
    (fn [nested-vector _]
      (conj nested-vector nested-vector))
    base-vector
    (range 0 dept)))


(deftest alternative-flatten-test
  (testing "Should return the expected result "

    (testing "for one level vector of numbers"
      (is (=
            [0 1 2 3 4 5 6 7 8 9]
            (alt-flatten [0 1 2 3 4 5 6 7 8 9]))))

    (testing "for vector with one element"
      (is (=
            [0]
            (alt-flatten [0]))))

    (testing "for empty vector"
      (is (=
            []
            (alt-flatten []))))

    (testing "for nested vectors"
      (is (=
            [ 0 1 2 3 4 5 6 7 8 9 10]
            (alt-flatten [0 1 2 3 [4 5 6 [ 7 8 ]] [9 10]])))))

  (testing "Should throw errors"

    (testing "when input is not a vector"
      (is (thrown?
            IllegalArgumentException
            (alt-flatten 0))))

    (testing "when vector argument contains elements that are not integers or vectors of integers"
      (is (thrown?
            IllegalArgumentException
            (alt-flatten [1 2 "3"])))

      (is (thrown?
            IllegalArgumentException
            (alt-flatten [1 2 ["3", 4]]))))))
